﻿namespace FoodRecognition.Core.Services.Dish.Models;

public class DishAbbreviated
{
    public string Id { get; set; } = null!;
    public double Price { get; set; } 
}