﻿namespace FoodRecognition.Core.Services.Dish.Models;

public class DishModel
{
   public List<DishAbbreviated> Dishes { get; set; } = null!;
   public double Sum { get; set; }

}
